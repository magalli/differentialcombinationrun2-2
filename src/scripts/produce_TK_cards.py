import os

"""
# Yukawa
print("HggHZZHtt Yukawa")

input_card = "DifferentialCombinationRun2/CombinedCards/TK/Yukawa_HggHZZHtt.txt"
output_card = "DifferentialCombinationRun2/CombinedCards/TK/Yukawa_HggHZZHtt_pruned.txt"

to_drop = [
    # Hgg
    "hgg_hgg_PTH_120p0_140p0",
    "hgg_hgg_PTH_140p0_170p0",
    "hgg_hgg_PTH_170p0_200p0",
    "hgg_hgg_PTH_200p0_250p0",
    "hgg_hgg_PTH_250p0_350p0",
    "hgg_hgg_PTH_350p0_450p0",
    "hgg_hgg_PTH_450p0_10000p0",
    # HZZ
    "hzz_hzz_hzz_PTH_120_200",
    "hzz_hzz_hzz_PTH_200_350",
    "hzz_hzz_hzz_PTH_350_600",
    "hzz_hzz_hzz_PTH_GT600",
    # Htt
    "htt_htt_PTH_120_200",
    "htt_htt_PTH_200_350",
    "htt_htt_PTH_350_450",
    "htt_htt_PTH_GT350",
    "htt_htt_PTH_GT450",
]

to_remove = [
    "120p0_140p0",
    "140p0_170p0",
    "170p0_200p0",
    "200p0_250p0",
    "250p0_350p0",
    "350p0_450p0",
    "450p0_10000p0",
]

nuis = ["ratesmH_PTH", "pdfindex"]
command = (
    "combineCards.py {} ".format(input_card)
    + " ".join(["--xc={}*".format(x) for x in to_drop])
    + " > {}".format(output_card)
)
print(command)
os.system(command)

new_lines = []
with open(output_card, "r") as f:
    lines = f.readlines()
    for line in lines:
        if line.startswith("imax"):
            line = "imax * number of bins\n"
            new_lines.append(line)
            continue
        if line.startswith("jmax"):
            line = "jmax * number of processes minus 1\n"
            new_lines.append(line)
            continue
        if line.startswith("kmax"):
            line = "kmax * number of nuisance parameter\n"
            new_lines.append(line)
            continue
        line = line.replace("ch1_", "")
        # For some reason that I fail to understand, DifferentialCombinationRun2/CombinedCards/smH_PTH is attached to the path
        # Remove it
        line = line.replace("DifferentialCombinationRun2/CombinedCards/TK/", "")
        # Remove stuff like CMS_fake_syst_em_PTH_GT200 from hww
        # if line.startswith("experimental"):
        #    new_words = []
        #    for word in line.split():
        #        if "GT200" not in word and "120_200" not in word:
        #            new_words.append(word)
        #    line = " ".join(new_words) + "\n"
        if any(tr in line for tr in to_remove) and any(n in line for n in nuis):
            continue
        else:
            new_lines.append(line)
with open(output_card, "w") as f:
    for line in new_lines:
        f.write(line)
"""

# Top

input_cards = (
    "DifferentialCombinationRun2/CombinedCards/TK/Top_HggHZZHttHttBoostHbbVBF.txt",
    "DifferentialCombinationRun2/Analyses/hig-21-017/BoostedHTT_DiffXS_HiggsPt_NoOverLap/V2_Diff_dr0p5_hpt_2bin/hig-21-017_hpt_xHNuisPar.txt",
)

output_cards = (
    "DifferentialCombinationRun2/CombinedCards/TK/Top_HggHZZHttHttBoostHbbVBF.txt",
    "DifferentialCombinationRun2/Analyses/hig-21-017/BoostedHTT_DiffXS_HiggsPt_NoOverLap/V2_Diff_dr0p5_hpt_2bin/hig-21-017_hpt_xHNuisPar_nomass.txt",
)

for input_card, output_card in zip(input_cards, output_cards):
    with open(input_card, "r") as f:
        lines = f.readlines()
        # replace $MASS with 125 in the datacard
        new_lines = []
        for line in lines:
            line = line.replace("$MASS", "125")
            new_lines.append(line)
        with open(output_card, "w") as f:
            for line in new_lines:
                f.write(line)

# prepend htt to channels in hig-21-017_hpt_xHNuisPar_nomass.txt
command = "combineCards.py httboost={}".format(output_cards[1]) + " > {}".format(
    output_cards[1].replace(".txt", "_prefix.txt")
)
print(command)
os.system(command)

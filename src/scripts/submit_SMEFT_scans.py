import argparse
import os
import time
from datetime import datetime
import shutil
import json

from differential_combination.command.SMEFT.base_commands import (
    AsimovWithEnvelope,
    Observed,
    Asimov,
)
from differential_combination.utils import (
    setup_logging,
    extract_from_yaml_file,
    create_and_access_nested_dir,
)


def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Submit scans for SMEFT interpretations"
    )

    parser.add_argument("--debug", action="store_true", help="Print debug messages")

    parser.add_argument(
        "--chan-obs",
        required=True,
        type=str,
        help="Path to a JSON file containing a dict channel:observable",
    )

    parser.add_argument(
        "--category",
        default="observed",
        type=str,
        help="Category for which submit scans",
        choices=["observed", "asimov", "statonly", "statonly_asimov"],
    )

    parser.add_argument(
        "--input-dir",
        type=str,
        help="Directory where the .root file containing the workspaces is stored",
    )

    parser.add_argument(
        "--output-dir",
        type=str,
        default="outputs",
        help="Directory where output files will be stored",
    )

    parser.add_argument(
        "--base-model",
        required=True,
        type=str,
        help="Path to the yaml file that was used to create the workspace",
    )

    parser.add_argument(
        "--submodel",
        required=True,
        type=str,
        help="If the name of a parameter, perform fit freezing all the others"
        "Otherwise it has to be the path to a yaml file with the name MODEL_SUBMODEL.yml",
    )

    parser.add_argument(
        "--skip-2d",
        action="store_true",
        required=False,
        default=False,
        help="Skip 2D scans",
    )

    parser.add_argument(
        "--skip-scans",
        action="store_true",
        required=False,
        default=False,
        help="Skip scans",
    )

    parser.add_argument(
        "--twod-only",
        action="store_true",
        required=False,
        default=False,
        help="Only run 2D scans",
    )

    parser.add_argument(
        "--global-fit-dir",
        type=str,
        default=None,
        help="Directory where the global fit is stored",
    )

    return parser.parse_args()


def main(args):
    if args.debug:
        logger = setup_logging(level="DEBUG")
    else:
        logger = setup_logging(level="INFO")
    logger.info("Starting scans submission for SMEFT interpretation")

    initial_path = os.path.abspath(os.getcwd())

    base_model_dct = extract_from_yaml_file(args.base_model)
    main_model_pois = list(base_model_dct.keys())
    model_name = args.base_model.split("/")[-1].split(".")[0]

    cat_dct = {
        "observed": "",
        "asimov": "_asimov",
        "statonly": "_statonly",
        "statonly_asimov": "_statonly_asimov",
    }

    chan_obs_name = args.chan_obs.split("/")[-1].split(".")[0]
    with open(args.chan_obs) as f:
        chan_obs_dct = json.load(f)
        channels = list(chan_obs_dct.keys())
        observables = list(chan_obs_dct.values())

    if args.input_dir.startswith("/"):
        input_dir = args.input_dir
    else:
        input_dir = "{}/{}".format(initial_path, args.input_dir)

    submodel_pois = None
    if args.submodel.endswith(".yml"):
        submodel_dct = extract_from_yaml_file(args.submodel)
        submodel_pois = [sp for sp in list(submodel_dct.keys()) if sp != "scan_ranges"]
        submodel_name = args.submodel.split("/")[-1].split(".")[0].split("_")[-1]
        pois_to_fit = submodel_pois

        # ranges
        substrings = [
            "{}={},{}".format(p, submodel_dct[p]["min"], submodel_dct[p]["max"])
            for p in submodel_pois
        ]
        set_parameter_ranges_string = "--setParameterRanges {}".format(
            ":".join(substrings)
        )
    else:
        submodel_name = "FreezeOthers"
        pois_to_fit = [args.submodel]
        set_parameter_ranges_string = ""

    # Pick routine and create
    which_routine = Observed
    if "hgg" in channels and "asimov" in args.category:
        which_routine = AsimovWithEnvelope
    elif "asimov" in args.category and "hgg" not in channels:
        which_routine = Asimov

    full_category = "{}{}".format(chan_obs_name, cat_dct[args.category])

    """
    job_specs = {
        "220926Atlas": {
            "Hgg_asimov": {
                "points_one_dim": 100,
                "split_points_one_dim": 4,
                "points_two_dim": 600,
                "split_points_two_dim": 4,
            },
            "HggHZZHWWHttHbbVBF_asimov": {
                "points_one_dim": 200,
                "split_points_one_dim": 2,
                "points_two_dim": 600,
                "split_points_two_dim": 2,
            },
            "HggHZZHWWHttHbbVBFHttBoost_asimov": {
                "points_one_dim": 200,
                "split_points_one_dim": 2,
                "points_two_dim": 600,
                "split_points_two_dim": 2,
            },
        },
        "221121PruneNoCPEVhgghzzhwwhtthbbvbfhttboost": {
            "HggHZZHWWHttHbbVBFHttBoost_asimov": {
                "points_one_dim": 300,
                "split_points_one_dim": 2,
                "points_two_dim": 600,
                "split_points_two_dim": 2,
            }
        },
        "221121PruneNoCPEVhgghzzhwwhtthbbvbfhttboostLinear": {
            "HggHZZHWWHttHbbVBFHttBoost_asimov": {
                "points_one_dim": 300,
                "split_points_one_dim": 2,
                "points_two_dim": 600,
                "split_points_two_dim": 2,
            }
        },
        "221121PruneNoCPEVhgghzzhwwhtthbbvbfhttboostNonLinear": {
            "HggHZZHWWHttHbbVBFHttBoost_asimov": {
                "points_one_dim": 400,
                "split_points_one_dim": 2,
                "points_two_dim": 600,
                "split_points_two_dim": 2,
            }
        },
        "221206GroupStudyEVhgghzzhwwhtthbbvbfhttboostEWPOBosonicYukawaRestchg": {
            "HggHZZHWWHttHbbVBFHttBoost_asimov": {
                "points_one_dim": 60,
                "split_points_one_dim": 2,
                "points_two_dim": 600,
                "split_points_two_dim": 2,
            },
            "HggHZZHWWHttHbbVBFHttBoost_asimov_statonly": {
                "points_one_dim": 300,
                "split_points_one_dim": 15,
                "points_two_dim": 600,
                "split_points_two_dim": 2,
            },
        },
        "221209Test": {
            "Hgg_asimov": {
                "points_one_dim": 200,
                "split_points_one_dim": 10,
                "points_two_dim": 600,
                "split_points_two_dim": 2,
            }
        },
        "221107StudyEVhgghzzhwwhttEWPOBosonicYukawachwLinear": {
            "HggHZZHWWHtt_asimov": {
                "points_one_dim": 300,
                "split_points_one_dim": 5,
                "points_two_dim": 600,
                "split_points_two_dim": 2,
            }
        },
    }
    """
    job_specs = {
        "220926Atlas": {
            "PtHgg": {
                "points_one_dim": 100,
                "split_points_one_dim": 4,
                "points_two_dim": 600,
                "split_points_two_dim": 4,
            },
            "PtHggHZZ_asimov": {
                "points_one_dim": 100,
                "split_points_one_dim": 4,
                "points_two_dim": 600,
                "split_points_two_dim": 4,
            },
            "PtFullComb_asimov": {
                "points_one_dim": 400,
                "split_points_one_dim": 1,
                "points_two_dim": 600,
                "split_points_two_dim": 2,
            },
            "DeltaPhiJJHggHZZ": {
                "points_one_dim": 100,
                "split_points_one_dim": 8,
                "points_two_dim": 600,
                "split_points_two_dim": 4,
            },
            "DeltaPhiJJHggHZZ_asimov": {
                "points_one_dim": 100,
                "split_points_one_dim": 8,
                "points_two_dim": 600,
                "split_points_two_dim": 4,
            },
        },
        "230611AtlasDPJ": {
            "DeltaPhiJJHggHZZ_asimov": {
                "points_one_dim": 200,
                "split_points_one_dim": 10,
                "points_two_dim": 800,
                "split_points_two_dim": 4,
            },
        },
    }

    try:
        points_one_dim = job_specs[model_name][full_category]["points_one_dim"]
        split_points_one_dim = job_specs[model_name][full_category][
            "split_points_one_dim"
        ]
        points_two_dim = job_specs[model_name][full_category]["points_two_dim"]
        split_points_two_dim = job_specs[model_name][full_category][
            "split_points_two_dim"
        ]
    except KeyError:
        logger.warning("No job specs found for {} {}".format(model_name, full_category))
        # points_one_dim = 100
        points_one_dim = 70
        split_points_one_dim = 1
        points_two_dim = 400
        split_points_two_dim = 1

    routine = which_routine(
        model_name,
        submodel_name,
        input_dir,
        chan_obs_name,
        main_model_pois=main_model_pois,
        pois_to_fit=pois_to_fit,
        submodel_pois=submodel_pois,
        skip_2d=args.skip_2d,
        skip_scans=args.skip_scans,
        set_parameter_ranges_string=set_parameter_ranges_string,
        twod_only=args.twod_only,
        statonly=True if "statonly" in full_category else False,
        skip_best=True if args.global_fit_dir else False,
        points_one_dim=points_one_dim,
        split_points_one_dim=split_points_one_dim,
        points_two_dim=points_two_dim,
        split_points_two_dim=split_points_two_dim,
    )

    logger.info("Built the following XSRoutine: \n{}".format(routine))
    logger.debug(
        "One line versions in case you want to copy:\n\n{}".format(
            "\n\n".join([c.full_command for c in routine.commands])
        )
    )
    # Create output subdirs and move to output dir
    if args.submodel.endswith(".yml"):
        output_path = "{}/{}/{}/{}-{}".format(
            args.output_dir,
            model_name,
            submodel_name,
            full_category,
            datetime.today().strftime("%Y%m%dxxx%H%M%S"),
        )
    else:
        output_path = "{}/{}/FreezeOthers_{}/{}-{}".format(
            args.output_dir,
            model_name,
            pois_to_fit[0],
            full_category,
            datetime.today().strftime("%Y%m%dxxx%H%M%S"),
        )

    new_output_path = create_and_access_nested_dir(output_path)
    if args.global_fit_dir:
        logger.info(
            "Copying global fit files from {} to {}".format(
                args.global_fit_dir, new_output_path
            )
        )
        if "hgg" in channels and "asimov" in full_category:
            for fl in [
                "higgsCombineAsimovBestFit.MultiDimFit.mH125.38.root",
                "higgsCombineAsimovPostFit.GenerateOnly.mH125.38.123456.root",
                "higgsCombineAsimovPreFit.GenerateOnly.mH125.38.123456.root",
            ]:
                os.system(
                    "cp {}/{} {}".format(args.global_fit_dir, fl, new_output_path)
                )
        else:
            global_fit_file = (
                "higgsCombine_POSTFIT_{}.MultiDimFit.mH125.38.root".format(
                    chan_obs_name
                )
            )
            os.system(
                "cp {}/{} {}".format(
                    args.global_fit_dir, global_fit_file, new_output_path
                )
            )
        logger.info("Waiting 5 seconds to make sure files are copied")
        time.sleep(5)
    if args.debug:
        logger.debug("Removing {} as it won't be used".format(new_output_path))
        # remove the directory with all the files
        shutil.rmtree(new_output_path)

    # Run routine (submit jobs)
    if not args.debug:
        routine.run()


if __name__ == "__main__":
    args = parse_arguments()
    main(args)

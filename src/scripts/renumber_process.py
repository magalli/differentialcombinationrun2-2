""" Given a datacard (flag --datacard) and a process name which is in it (--process) turn the number of that process from 
signal to background and viceversa.
The number assigned will be max(numbers) + 1 if the process is initially a 
"""
import argparse
from copy import copy


def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Change a process from signal to background or viceversa in a datacard"
        )

    parser.add_argument(
        "--datacard",
        required=True,
        type=str
    )

    parser.add_argument(
        "--process",
        required=True,
        type=str
    )

    return parser.parse_args()


def main(args):
    datacard = args.datacard
    process = args.process

    with open(datacard, "r") as f:
        lines = f.readlines()
    
    # Find lines starting with 'process' and their index
    two_lines = {}
    for idx, line in enumerate(lines):
        if line.startswith("process"):
            two_lines[idx] = line.split()

    # Numbers of processes
    numbers = [int(string_num) for string_num in two_lines[max(two_lines.keys())][1:]]
    
    # Loop over zipped lists
    new_second_line = copy(two_lines[max(two_lines.keys())])
    for idx, (name, number) in enumerate(zip(two_lines[min(two_lines.keys())], two_lines[max(two_lines.keys())])):
        if name == process:
            if int(number) <= 0: # signal
                new_second_line[idx] = str(max(numbers) + 1)
            else: # bkg
                new_second_line[idx] = str(min(numbers) - 1)
    two_lines[max(two_lines.keys())] = new_second_line

    # Write
    new_lines = copy(lines)
    for idx, lst in two_lines.items():
        new_lines[idx] = " ".join(lst) + "\n"
    
    output_file = datacard.replace(".txt", "_renumbered.txt")
    with open(output_file, "w") as f:
        f.writelines(new_lines)



if __name__ == "__main__":
    args = parse_arguments()
    main(args)
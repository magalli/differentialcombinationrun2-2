""" Script to submit point scans for differential cross sections.
Following are the main parameters:
- observable: observable we are working with
- category: as can be seen in the README, it's usually something that identifies a combination of
year and decay channel, e.g. 2017_Hgg, 2018_Hzz, 2016_Combination, etc.
- metadata-dir: path to the directory where yaml files (1 per category) containing combine+combineTool
arguments specifications are stored
- input-dir: directory where workspaces are stored
- output-dir: base output directory; in this directory, the subdirectories created will be 
    xs_plots/observable/category
if category is already present, category-1, catyegory-2, etc. will be created, and the output
root files will be stored there
- single-poi: in case we want to submit scans for only one POI, specify it with this flag
- level: either INFO or DEBUG
- global-fit-file: if provided, the global fit will not be run again, but the file will be copied
"""
import argparse
import os
from datetime import datetime

from differential_combination.utils import setup_logging
from differential_combination.command import commands
from differential_combination.utils import (
    get_nested_keys,
    extract_from_yaml_file,
    create_and_access_nested_dir,
)


def parse_arguments():
    parser = argparse.ArgumentParser(description="Submit scans")

    parser.add_argument("--debug", action="store_true", help="Print debug messages")

    parser.add_argument(
        "--model",
        required=True,
        type=str,
        choices=list(commands.keys()),
        help="Model considered",
    )

    parser.add_argument(
        "--observable",
        required=True,
        type=str,
        choices=list(commands["SM"].keys()),
        help="observable for which to run differential xs scans",
    )

    parser.add_argument(
        "--category",
        required=True,
        type=str,
        choices=get_nested_keys(commands["SM"]),
        help="Category for which submit scans",
    )

    parser.add_argument(
        "--input-dir",
        type=str,
        help="Directory where the .root file containing the workspaces is stored",
    )

    parser.add_argument(
        "--output-dir",
        type=str,
        default="outputs",
        help="Directory where output files will be stored",
    )

    parser.add_argument(
        "--single-poi",
        type=str,
        help="Specify the name of the poi if the scan for only one parameter wants to be run",
    )

    parser.add_argument(
        "--global-fit-file",
        type=str,
        help="Specify the name of the global fit in order not to run it again",
    )

    return parser.parse_args()


def main(args):
    if args.debug:
        logger = setup_logging(level="DEBUG")
    else:
        logger = setup_logging(level="INFO")
    logger.info("Starting scans submission")

    initial_path = os.path.abspath(os.getcwd())

    # Assign from args
    model = args.model
    observable = args.observable
    category = args.category

    if args.input_dir.startswith("/"):
        input_dir = args.input_dir
    else:
        input_dir = "{}/{}".format(initial_path, args.input_dir)
    output_dir = args.output_dir

    logger.info("Observable: {}".format(observable))
    logger.info("Channel: {}".format(category))
    logger.info("Output Directory: {}".format(output_dir))

    # Build Routine
    if args.single_poi:
        pois = [args.single_poi]
    else:
        # badly hardcoded, I hate myself for this
        pois_dir = "/work/gallim/DifferentialCombination_home/DifferentialCombinationRun2/metadata/xs_POIs"
        pois = extract_from_yaml_file(
            "{}/{}/{}/{}.yml".format(
                pois_dir, model, observable, category.split("_")[0]
            )
        )

    global_fit_file = args.global_fit_file
    if args.global_fit_file:
        if args.global_fit_file.startswith("/"):
            global_fit_file = args.global_fit_file
        else:
            global_fit_file = "{}/{}".format(initial_path, args.global_fit_file)
        logger.info(
            "Global fit file provided: {}, global fit will be skipped!".format(
                global_fit_file
            )
        )

    routine = commands[model][observable][category](
        input_dir, pois, global_fit_file=global_fit_file
    )

    logger.info("Built the following XSRoutine: \n{}".format(routine))
    logger.debug(
        "One line versions in case you want to copy:\n\n{}".format(
            "\n\n".join([c.full_command for c in routine.commands])
        )
    )

    # Create output subdirs and move to output dir
    output_path = "{}/{}/{}-{}".format(
        output_dir, observable, category, datetime.today().strftime("%Y%m%dxxx%H%M%S")
    )

    new_output_path = create_and_access_nested_dir(output_path)
    if args.debug:
        logger.debug("Removing {} as it won't be used".format(new_output_path))
        os.rmdir(new_output_path)

    # If the global fit file is provided, we need to copy it in the newly created output dir
    if global_fit_file and not args.debug:
        logger.info(
            "Copying global fit file {} to {}".format(global_fit_file, new_output_path)
        )
        os.system("cp {} {}".format(global_fit_file, new_output_path))

    # Run routine (submit jobs)
    if not args.debug:
        routine.run()


if __name__ == "__main__":
    args = parse_arguments()
    main(args)

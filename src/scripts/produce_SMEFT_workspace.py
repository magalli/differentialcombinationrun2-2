import argparse
import pprint
import os
import yaml

from differential_combination.utils import (
    setup_logging,
    mkdir,
    extract_from_yaml_file,
    pretty_ordered_dict,
)


def parse_arguments():
    parser = argparse.ArgumentParser(description="Produce workspaces")

    parser.add_argument("--debug", action="store_true", help="Print debug messages")

    parser.add_argument(
        "--datacard", required=True, type=str, help="Datacard converted to workspace"
    )

    parser.add_argument(
        "--config-file",
        required=True,
        type=str,
        help="Path to a yaml file containing the model",
    )

    parser.add_argument(
        "--equations-dir",
        required=True,
        type=str,
        help="Path to a directory in Matt's repo containing the equations",
    )

    parser.add_argument(
        "--chan-obs",
        required=True,
        type=str,
        help="Path to a JSON file containing a dict channel:observable",
    )

    parser.add_argument("--linear-only", action="store_true", help="Linear only")
    parser.add_argument("--linearised", action="store_true", help="Linear only")
    parser.add_argument("--constant-mass", action="store_true", help="Constant MH")

    parser.add_argument(
        "--output-dir",
        required=False,
        default="DifferentialCombinationRun2/CombinedWorkspaces/SMEFT",
        type=str,
        help="Output directory",
    )

    parser.add_argument(
        "--validate",
        action="store_true",
        default=False,
        help="Print the command without running it",
    )

    return parser.parse_args()


def main(args):
    if args.debug:
        logger = setup_logging(level="DEBUG")
    else:
        logger = setup_logging(level="INFO")
    pp = pprint.PrettyPrinter(indent=4)

    model_name = args.config_file.split("/")[-1].split(".")[0]
    logger.info("Model name: {}".format(model_name))

    chan_obs_name = args.chan_obs.split("/")[-1].split(".")[0]
    output_dir = os.path.join(args.output_dir, model_name)
    mkdir(output_dir)
    output_file = "{}.root".format(chan_obs_name)

    command = [
        "text2workspace.py",
        args.datacard,
        "-o {}/{}".format(output_dir, output_file),
        "-P HiggsAnalysis.CombinedLimit.DIFFtoSMEFTModel:diff_to_smeft_model",
        "--PO input_dir={}".format(args.equations_dir),
        "--PO config_file={}".format(args.config_file),
        "--PO chan_obs_file={}".format(args.chan_obs),
        "--PO linear_only" if args.linear_only else "",
        "--PO linearised" if args.linearised else "",
        "--PO constant_mass" if args.constant_mass else "",
    ]

    logger.info("Command: \n{}".format(pp.pformat(command)))

    if args.validate:
        logger.info(" ".join(command))
    else:
        os.system(" ".join(command))
        with open(os.path.join(output_dir, "{}.txt".format(chan_obs_name)), "w") as f:
            f.write(" ".join(command))


if __name__ == "__main__":
    args = parse_arguments()
    main(args)

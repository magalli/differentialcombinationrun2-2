from ..command import Command, Routine
from smH_PTH import AsimovWithEnvelope, acm_string
from smH_PTH import Hgg as smH_PTH_Hgg

all_pois_Hgg = [
    "r_smH_PTJ0_0_30",
    "r_smH_PTJ0_30_40",
    "r_smH_PTJ0_40_55",
    "r_smH_PTJ0_55_75",
    "r_smH_PTJ0_75_95",
    "r_smH_PTJ0_95_120",
    "r_smH_PTJ0_120_150",
    "r_smH_PTJ0_150_200",
    "r_smH_PTJ0_GT200",
]

all_pois_HZZ = [
    "r_smH_PTJ0_0_30",
    "r_smH_PTJ0_30_55",
    "r_smH_PTJ0_55_95",
    "r_smH_PTJ0_95_200",
    "r_smH_PTJ0_GT200",
]

all_pois_Htt = [
    "r_smH_PTJ0_0_30",
    "r_smH_PTJ0_30_60",
    "r_smH_PTJ0_60_120",
    "r_smH_PTJ0_120_200",
    "r_smH_PTJ0_200_350",
    "r_smH_PTJ0_GT350",
]

all_pois_HttBoost = ["r_smH_PTJ0_450_600", "r_smH_PTJ0_GT600"]

all_pois_HggHZZHttBoost = [
    "r_smH_PTJ0_0_30",
    "r_smH_PTJ0_30_40",
    "r_smH_PTJ0_40_55",
    "r_smH_PTJ0_55_75",
    "r_smH_PTJ0_75_95",
    "r_smH_PTJ0_95_120",
    "r_smH_PTJ0_120_150",
    "r_smH_PTJ0_150_200",
    "r_smH_PTJ0_200_450",
    "r_smH_PTJ0_450_600",
    "r_smH_PTJ0_GT600",
]


class Hgg_asimov(AsimovWithEnvelope):
    all_pois = all_pois_Hgg
    task_name = "Hgg_asimov"
    n_points = 40
    split_points = 10

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(Hgg_asimov, self).__init__(
            input_dir, pois, name="Hgg", global_fit_file=global_fit_file
        )


class Hgg_asimov_statonly(Hgg_asimov):
    task_name = "Hgg_asimov_statonly"

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(Hgg_asimov_statonly, self).__init__(
            input_dir, pois, global_fit_file=global_fit_file
        )
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")


class Hgg(smH_PTH_Hgg):
    all_pois = all_pois_Hgg
    task_name = "Hgg"
    n_points = 40
    split_points = 10

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(Hgg, self).__init__(input_dir, pois, global_fit_file=global_fit_file)


class Hgg_statonly(Hgg):
    task_name = "Hgg_statonly"

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(Hgg_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")


class HZZ(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        for poi in pois:
            poi_command = Command(
                executable="combine",
                input_file="{}/HZZ.root".format(input_dir),
                args=[
                    "--name _SCAN_{}_HZZ".format(poi),
                    "-m 125.38",
                    "--algo=grid",
                    "--freezeParameters MH",
                    "--cminDefaultMinimizerStrategy 0",
                    "--floatOtherPOIs=1",
                    "--method MultiDimFit",
                    "--points 60",
                    "--saveWorkspace",
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois_HZZ])
                    ),
                    "-P {}".format(poi),
                    "--redefineSignalPOIs {}".format(poi),
                ],
            )
            self.commands.append(poi_command)


class HZZ_asimov(HZZ):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HZZ_asimov, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--toys -1")


class Htt(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        self.name = "Htt"
        global_fit_command = Command(
            executable="combine",
            input_file="{}/{}.root".format(input_dir, self.name),
            args=[
                "--name _POSTFIT_{}".format(self.name),
                "-M MultiDimFit",
                "-m 125",
                "--robustFit=1",
                "--X-rtd MINIMIZER_analytic",
                "--algo=singles",
                "--cl=0.68",
                "--setParameters {}".format(
                    ",".join(["{}=1".format(p) for p in self.all_pois])
                ),
                "--setParameterRanges {}".format(
                    ":".join(["{}=-5,5".format(p) for p in self.all_pois])
                ),
                "--redefineSignalPOIs {}".format(",".join(all_pois_Htt)),
                "--floatOtherPOIs=1",
                "--cminDefaultMinimizerStrategy=0",
                "--saveFitResult",
            ],
        )
        self.commands.append(global_fit_command)
        for poi in pois:
            poi_command = Command(
                executable="combineTool.py",
                input_file="{}/{}.root".format(input_dir, self.name),
                args=[
                    "--name _SINGLES_{}_{}".format(poi, self.name),
                    "-M MultiDimFit",
                    "-m 125",
                    "--robustFit=1",
                    "--X-rtd MINIMIZER_analytic",
                    "--algo=singles",
                    "--cl=0.68",
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in self.all_pois])
                    ),
                    "--setParameterRanges {}".format(
                        ":".join(["{}=-5,5".format(p) for p in self.all_pois])
                    ),
                    "--redefineSignalPOIs {}".format(",".join(all_pois_Htt)),
                    "--floatOtherPOIs=1",
                    "--cminDefaultMinimizerStrategy=0",
                    "-P {}".format(poi),
                    "--job-mode slurm",
                    "--task-name _SCAN_{}_{}".format(poi, self.name),
                ],
            )
            self.commands.append(poi_command)


class Htt_asimov(Htt):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(Htt_asimov, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--toys -1")


class HttBoost(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        for poi in pois:
            poi_command = Command(
                executable="combine",
                input_file="{}/HttBoost.root".format(input_dir),
                args=[
                    "--name _SINGLES_{}_HttBoost".format(poi),
                    "-m 125",
                    "--algo=singles",
                    "--robustFit 1",
                    "--X-rtd MINIMIZER_analytic",
                    "--cl=0.68",
                    "--floatOtherPOIs=1",
                    "--cminDefaultMinimizerStrategy=0",
                    "--method MultiDimFit",
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois_HttBoost])
                    ),
                    "--redefineSignalPOIs {}".format(poi),
                    "-P {}".format(poi),
                ],
            )
            self.commands.append(poi_command)


class HttBoost_asimov(HttBoost):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HttBoost_asimov, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--toys -1")


class HggHZZHttBoost_asimov(AsimovWithEnvelope):
    all_pois = all_pois_HggHZZHttBoost
    task_name = "HggHZZHttBoost_asimov"
    n_points = 40
    split_points = 5

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHZZHttBoost_asimov, self).__init__(
            input_dir, pois, name="HggHZZHttBoost", global_fit_file=global_fit_file
        )


class HggHZZHttBoost(smH_PTH_Hgg):
    all_pois = all_pois_HggHZZHttBoost
    name = "HggHZZHttBoost"
    task_name = "HggHZZHttBoost"
    n_points = 40
    split_points = 5

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHZZHttBoost, self).__init__(
            input_dir, pois, global_fit_file=global_fit_file
        )


class HggHZZHttBoost_statonly(Hgg):
    task_name = "HggHZZHttBoost_statonly"

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHZZHttBoost_statonly, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")

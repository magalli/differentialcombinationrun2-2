from ..command import Command, Routine
from smH_PTH import AsimovWithEnvelope, acm_string
from smH_PTH import Hgg as smH_PTH_Hgg

all_pois_Hgg = [
    "r_mjj_out",
    "r_mjj_0_75",
    "r_mjj_75_120",
    "r_mjj_120_180",
    "r_mjj_180_300",
    "r_mjj_300_500",
    "r_mjj_500_1000",
    "r_mjj_GT1000",
]

all_pois_HZZ = ["r_mjj_out", "r_mjj_0_120", "r_mjj_120_300", "r_mjj_GT300"]


class Hgg_asimov(AsimovWithEnvelope):
    all_pois = all_pois_Hgg
    task_name = "Hgg_asimov"

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(Hgg_asimov, self).__init__(
            input_dir, pois, name="Hgg", global_fit_file=global_fit_file
        )


class Hgg(smH_PTH_Hgg):
    all_pois = all_pois_Hgg
    task_name = "Hgg"

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(Hgg, self).__init__(input_dir, pois, global_fit_file=global_fit_file)


class HZZ(Routine):
    def __init__(self, input_dir, pois, global_fit_file=None):
        self.commands = []
        for poi in pois:
            poi_command = Command(
                executable="combine",
                input_file="{}/HZZ.root".format(input_dir),
                args=[
                    "--name _SCAN_{}_HZZ".format(poi),
                    "-m 125.38",
                    "--algo=grid",
                    "--freezeParameters MH",
                    "--cminDefaultMinimizerStrategy 0",
                    "--floatOtherPOIs=1",
                    "--method MultiDimFit",
                    "--points 60",
                    "--saveWorkspace",
                    "--setParameters {}".format(
                        ",".join(["{}=1".format(p) for p in all_pois_HZZ])
                    ),
                    "-P {}".format(poi),
                    "--redefineSignalPOIs {}".format(poi),
                ],
            )
            self.commands.append(poi_command)


class HZZ_asimov(HZZ):
    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HZZ_asimov, self).__init__(input_dir, pois, global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--toys -1")


class HggHZZ_asimov(AsimovWithEnvelope):
    all_pois = all_pois_Hgg
    task_name = "HggHZZ_asimov"

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHZZ_asimov, self).__init__(
            input_dir, pois, name="HggHZZ", global_fit_file=global_fit_file
        )


class HggHZZ(smH_PTH_Hgg):
    all_pois = all_pois_Hgg
    name = "HggHZZ"
    task_name = "HggHZZ"
    n_points = 40
    split_points = 4

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHZZ, self).__init__(input_dir, pois, global_fit_file=global_fit_file)


class HggHZZ_statonly(HggHZZ):
    all_pois = all_pois_Hgg
    name = "HggHZZ"
    task_name = "HggHZZ_statonly"
    n_points = 40
    split_points = 4

    def __init__(self, input_dir, pois, global_fit_file=None):
        super(HggHZZ, self).__init__(input_dir, pois, global_fit_file=global_fit_file)
        for command in self.commands:
            command.add_or_replace_option("--freezeParameters allConstrainedNuisances")

"""
Given a list of datacards as input to the argument --datacards, perform the following operations:
- dump a JSON file ("summary.json" in the current directory) containing a dictionary in the form {"datacard": {"number": "process"}}
- print a set (i.e., all elements not repeated) of all the processes found in all the datacards provided
"""
import argparse
from itertools import combinations

from datacard_to_pyhf import datacard_to_json
from datacard_to_pyhf import json_str



def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Compare datacards"
        )

    parser.add_argument(
        "--datacards",
        required=True,
        type=str,
        nargs="+"
    )

    return parser.parse_args()


def main(args):
    datacards_paths = args.datacards

    datacards = []
    for path in datacards_paths:
        datacards.append({"name": path.split("/")[-1], "path": path})

    for datacard in datacards:
        with open(datacard["path"]) as f:
            lines = f.readlines()
        datacard["ws"] = datacard_to_json(lines)

    # Dump a json file containing a dicitonary in the form {"datacard": {"number": "process"}}
    ids = {}
    for datacard in datacards:
        #print("Datacard {} has channels {}\n".format(datacard["name"], [channel["name"] for channel in datacard["ws"]["channels"]]))
        #for channel in datacard["ws"]["channels"]:
            #print("Channel {} has processes {}".format(channel["name"], [sample["name"] for sample in channel["samples"]]))
        ws = datacard["ws"]
        ids_chan = {}
        for channel in ws["channels"]:
            for sample in channel["samples"]:
                id = int(sample["id"])
                name = sample["name"]
                if id not in ids_chan:
                    ids_chan[id] = name
                else:
                    if ids_chan[id] != name:
                        # Raise an error if, inside a certain datacard, a certain process has different number across the channels
                        raise AttributeError("In datacard {} there is a mismatch of processes name/number".format(datacard["name"]))
        ids[datacard["name"]] = ids_chan
    
    with open("summary.json", "w") as f:
        f.write(json_str(ids))

    # Print a set (i.e., all elements not repeated) of all the processes found in all the datacards provided
    processes = []
    for dc in ids.values():
        processes.extend(list(dc.values()))
    unrepeated_processes = list(set(processes))
    unrepeated_processes.sort()
    print("Processes: {}".format(json_str(unrepeated_processes)))



if __name__ == "__main__":
    args = parse_arguments()
    main(args)
# Content

- [Installation](installation.md)
- [Repo Structure](repo_structure.md)
- [Signal Strength Fits](workflow_summary.md)
- [SMEFT Interpretation](workflow_summary_eft.md)
- [k-framework Interpretation](workflow_summary_tk.md)
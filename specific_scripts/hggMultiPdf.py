"""
To be called from inside 
/work/gallim/DifferentialCombination_home/DifferentialCombinationRun2/Analyses/hig-19-016/outdir_differential_Pt
with the following command:

python ../../../specific_scripts/hggMultiPdf.py --input Datacard_13TeV_differential_Pt.txt

it produces a file multipdfs_used.txt in the current directory (can be customized)
"""

import sys, os
import gzip, re
from subprocess import call, check_output

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', help='Input datacard')
parser.add_argument('-o', '--output', help='Output results []',default="multipdfs_used.txt")
args = parser.parse_args()
import ROOT
ROOT.gSystem.Load("libHiggsAnalysisCombinedLimit.so")

tmpdir="/tmp/"+os.environ["USER"] + "/hggMultiPdf"
base=os.environ['PWD']
if args.input[0] == '/': base=''

combineoptions="--cminFallbackAlgo Minuit2,Migrad,0:0.1 --X-rtd MINIMIZER_analytic --X-rtd FAST_VERTICAL_MORPH --X-rtd OPTIMIZE_BOUNDS=0 --X-rtd MINIMIZER_freezeDisassociatedParams --X-rtd NO_INITIAL_SNAP --X-rtd SIMNLL_GROUPCONSTRAINTS=10"
sigmas=3

## open datacard and parse: bin and pdfindex
bins=None
discrete=[]
workspaces={} # cat -> multipdf workspace
root_files = {}
with open(args.input, "r") as f_in:
    for line in f_in: 
        if len(line.split()) == 0:
            continue
        if re.match("bin",line) and bins==None:
            bins=line.split()[1:]
        if len(line.split()) >1 and line.split()[1] == 'discrete':
            discrete.append( line.split()[0])
        if line.split()[0] == 'shapes' and line.split()[1] == 'data_obs':
            # cat                        ->
            root_files[line.split()[2]] = line.split()[3]
            workspaces[line.split()[2]] = line.split()[4].split(':')[0]

def mycall(cmd):
    print "> Calling cmd:",cmd
    st=call(cmd,shell=True)
    if st!=0: 
        print "Error. Unable to run command: '"+cmd+"'"
        raise RuntimeError()

## save output here
out=open(args.output,"w")
total = {"used":0.,"total":0.}

for icat,cat in enumerate(bins):
    print " --- Doing ", cat ," ----"
    print "> ",icat,"/",len(bins)
    inputroot = root_files[cat]
    pdfindex_name = "pdfindex_"+ cat
    year = cat.split("_")[-2]
    pdfindex_name = pdfindex_name.replace("{}_".format(year), "{}_{}_".format(year, year))
    print("pdfindex:", pdfindex_name)
    s={"tmpdir":tmpdir,"cat":cat,"base":base,"input":args.input,"inputroot":inputroot,"combineoptions":combineoptions,"range":"--autoBoundsPOIs r"}
    s['discrete'] = pdfindex_name
    cmd = "rm -r %(tmpdir)s ; mkdir -p %(tmpdir)s"%s
    mycall(cmd)
    #copy card and inputs
    cmd="cd %(tmpdir)s && cp -v %(base)s/%(input)s ./ && cp -v %(base)s/CMS-HGG_* ./" %s
    mycall(cmd)
    ## combineCards
    cmd="cd %(tmpdir)s && combineCards.py --ic %(cat)s %(input)s > %(cat)s.txt"%s
    mycall(cmd)
    
    # remove other pdf_index and nuisances that do not belong
    allowed_binnings = []
    with open("{}/{}.txt".format(tmpdir, cat), "r") as f_in:
        lines = f_in.readlines()
        for line in lines:
            if line.startswith("process"):
                for process in line.split()[1:]:
                    try:
                        binning = "_".join(process.split("_")[2:])
                        if binning not in allowed_binnings and binning != "":
                            allowed_binnings.append(binning)
                    except:
                        continue
                break
    print("Allowed binnings: {}".format(allowed_binnings))
    new_lines = []
    with open("{}/{}.txt".format(tmpdir, cat), "r") as f_in:
        lines = f_in.readlines()
        found_rate_parameters = []
        for line in lines:
            if line.startswith("ratesmH_PTH") and not any(p in line.split()[0] for p in allowed_binnings):
                continue
            else:
                found_rate_parameters.append(line.split()[0])
            if line.startswith("pdfindex") and line.split()[0] != pdfindex_name:
                continue
            # remove e.g. nuisance edit  freeze ratesmH_PTH_5p0_10p0_17 when year is 16
            if line.startswith("nuisance") and line.split()[3] not in found_rate_parameters:
                continue
            new_lines.append(line)
    with open("{}/{}.txt".format(tmpdir, cat), "w") as f_out:
        f_out.writelines(new_lines)

    ## text2workspace
    cmd="cd %(tmpdir)s && text2workspace.py --for-fits --no-wrappers -m 125.38 %(cat)s.txt"%s
    mycall(cmd)
    ## combine: fit datacard to derive best fit and 68% intervals
    cmd="cd %(tmpdir)s && combine -M MultiDimFit -t 0 -m 125.38 -d %(cat)s.root --algo singles %(combineoptions)s %(range)s"%s
    mycall(cmd)
    ### fetch 
    fin = ROOT.TFile.Open("%(tmpdir)s/higgsCombineTest.MultiDimFit.mH125.38.root"%s)
    tin = fin.Get("limit")
    bf,lo,hi =None,None,None
    for i in range(0,tin.GetEntries()):
        tin.GetEntry(i)
        if abs(tin.quantileExpected +1)<0.01: bf = tin.r
        if  abs(tin.quantileExpected +0.32)<0.01: lo = tin.r
        if  abs(tin.quantileExpected -0.32)<0.01: hi = tin.r
    if bf == None or lo==None or hi==None:
        print "ERROR:","Unable to determine bf and err:",bf,lo,hi
        tin.Scan("r:quantileExpected")
        #raise RuntimeError()
        out.write("IDX: " + s['discrete'] + " FAILED\n")
        continue
    fin.Close()
    ## run grid
    elo = bf -lo
    ehi = hi -bf
    s["range"] = "--setParameterRanges r=%.2f,%.2f"%( min(bf-elo*sigmas, 0.8),max(1.2,bf+hi*sigmas)  )
    ## save index
    cmd="cd %(tmpdir)s && combine -M MultiDimFit --points 50 -t 0 -m 125.38 -d %(cat)s.root -n .scan --saveSpecifiedIndex %(discrete)s --algo grid %(combineoptions)s %(range)s"%s
    mycall(cmd)

    fin = ROOT.TFile.Open("%(tmpdir)s/higgsCombine.scan.MultiDimFit.mH125.38.root"%s)
    tin = fin.Get("limit")
    indexes=[]
    for i in range(0,tin.GetEntries()):
        tin.GetEntry(i) 
        indexes.append( eval("tin." + s['discrete']) )
    fin.Close()
    fin = ROOT.TFile.Open("%(tmpdir)s/%(inputroot)s"%s)
    ##
    print "> using workspace",workspaces[s['cat']]
    #w = fin.Get("multipdf")
    w = fin.Get(workspaces[s['cat']])
    pdfindex=w.cat(s['discrete'])
    if pdfindex == None:
        print "index is none:",s['discrete']
    nbins= pdfindex.numBins("")
    fin.Close()

    indexesshort=list(set(indexes))
    total['used'] += len(indexesshort)
    total['total'] += nbins

    out.write("IDX: " + s['discrete'] )
    to_add = s['discrete'].replace("pdfindex_","")
    out.write(" PDF: CMS_hgg_{}_bkgshape".format(to_add)) #CMS_hgg_RECO_0J_PTH_0_10_Tag0_13TeV_bkgshape
    out.write(" WSP: " + workspaces[s['cat']])
    out.write(" ROOTFILE: " + s['inputroot'])
    out.write(" LIST: " + ','.join(['%d'%x for x in indexesshort]) )
    out.write(" of a total of %d. "%nbins)
    out.write("Ratio %.0f %%"%(float(len(indexesshort))/float(nbins)*100.) )
    out.write("\n")
    #sys.exit(0) ### debug, only one cat

out.write("--------------\n")
out.write("TOT: USED %.0f / %.0f. Ratio %.0f %%\n"%(total['used'],total['total'],total['used']/total['total'] *100.))
out.write("--------------\n")
## 
#for i in xrange(self.cat.numBins("")):
#    self.cat.setIndex(i);
#    self.channels.append(self.cat.getLabel())
import json
from copy import deepcopy


class DatacardParser:
    def __init__(self, card):
        self.dct = {}
        self.processes = []
        self.processes_numbers = []
        self.channels = []
        self.other_lines = []
        self.not_necessary = {}
        self.rate_params = []
        # rateparams
        with open(
            "/work/gallim/DifferentialCombination_home/DifferentialCombinationRun2/TKPredictions/hgg.json",
            "r",
        ) as f:
            self.split = json.load(f)

        # read text file
        with open(card, "r") as f:
            lines = f.readlines()
            for n_line, line in enumerate(lines):
                if line.startswith("shapes"):
                    shapes_name, process, channel, file_path, ws_name = line.split()

                    # add channel to dict if not already there
                    if channel not in self.dct:
                        self.dct[channel] = {}
                    # add process to dict if not already there
                    if process not in self.dct[channel]:
                        self.dct[channel][process] = {}
                        self.dct[channel][process]["file"] = file_path
                        self.dct[channel][process]["ws"] = ws_name

                # now do stuff with the four lines starting respectively with
                # bin
                # process
                # process
                # rate
                if (
                    line.startswith("bin")
                    and lines[n_line + 1].startswith("process")
                    and lines[n_line + 2].startswith("process")
                    and lines[n_line + 3].startswith("rate")
                ):
                    line_bin = line
                    line_process = lines[n_line + 1]
                    line_is_signal = lines[n_line + 2]
                    line_rate = lines[n_line + 3]
                    self.channels = line_bin.split()[1:]
                    self.processes = line_process.split()[1:]
                    self.processes_numbers = line_is_signal.split()[1:]
                    rates = line_rate.split()[1:]

                    for channel, process, is_signal, rate in zip(
                        self.channels,
                        self.processes,
                        self.processes_numbers,
                        rates,
                    ):
                        if channel not in self.dct:
                            self.dct[channel] = {}
                        if process not in self.dct[channel]:
                            self.dct[channel][process] = {}
                        self.dct[channel][process]["process_number"] = is_signal
                        self.dct[channel][process]["rate"] = rate

                # systematics part
                if "lnN" in line:
                    name, tp, *contributions = line.split()
                    for channel, process, contribution in zip(
                        self.channels, self.processes, contributions
                    ):
                        if channel not in self.dct:
                            self.dct[channel] = {}
                        if process not in self.dct[channel]:
                            self.dct[channel][process] = {}
                        if "systematics" not in self.dct[channel][process]:
                            self.dct[channel][process]["systematics"] = {}
                        self.dct[channel][process]["systematics"][name] = contribution

                # rateParams
                if "rateParam" in line:
                    self.rate_params.append(line)

                # other lines
                # do not include the ones like
                # nuisance edit  freeze ratesmH_PTH_80p0_100p0_17
                # since they are related to rateParams
                if any(word in line for word in ["discrete", "param"]) and n_line > 7:
                    self.other_lines.append(line)

        # now in each channel remove processes with no process_number
        for channel in self.dct:
            self.not_necessary[channel] = {}
            for process in list(self.dct[channel]):
                if (
                    "process_number" not in self.dct[channel][process]
                    and process != "data_obs"
                ):
                    self.not_necessary[channel][process] = deepcopy(
                        self.dct[channel][process]
                    )
                    del self.dct[channel][process]
                    # remove also from self.processes, self.processes_numbers and the corresponding entries in self.channels
                    # consider that it can be there more than once
                    while process in self.processes:
                        i = self.processes.index(process)
                        del self.processes[i]
                        del self.processes_numbers[i]
                        del self.channels[i]

        print(json.dumps(self.dct, indent=4))

    def add_process(
        self, process, channel, file_path, ws_name, is_signal, rate, systematics
    ):
        # renumber considering that signal processes get numbers < 1 and background processes get numbers >= 1
        are_signal = [
            int(self.processes_numbers[i]) < 1
            for i in range(len(self.processes_numbers))
        ]
        self.processes.append(process)
        self.processes_numbers.append(is_signal)
        self.channels.append(channel)
        are_signal.append(is_signal)
        # now reassign numbers to processes
        is_signal_counter = 0
        isnot_signal_counter = 1
        self.processes_numbers = []
        process_number = {}
        for process, is_signal in zip(self.processes, are_signal):
            if process in process_number:
                self.processes_numbers.append(process_number[process])
            else:
                if is_signal:
                    self.processes_numbers.append(is_signal_counter)
                    process_number[process] = is_signal_counter
                    is_signal_counter -= 1
                else:
                    self.processes_numbers.append(isnot_signal_counter)
                    process_number[process] = isnot_signal_counter
                    isnot_signal_counter += 1

        # now add to dict
        if channel not in self.dct:
            self.dct[channel] = {}
        if process not in self.dct[channel]:
            self.dct[channel][process] = {}
        self.dct[channel][process]["file"] = file_path
        self.dct[channel][process]["ws"] = ws_name
        self.dct[channel][process]["rate"] = rate
        self.dct[channel][process]["systematics"] = systematics

        # now reassign numbers to processes in the dict
        for process, channel, number in zip(
            self.processes, self.channels, self.processes_numbers
        ):
            self.dct[channel][process]["process_number"] = number

    def write_card(self, card_out):
        with open(card_out, "w") as f:
            # First part (crap)
            f.write("imax * number of channels\n")
            f.write("jmax * number of processes minus 1\n")
            f.write("kmax * number of nuisance parameters\n")

            # Second part (shapes)
            f.write("-" * 100 + "\n")
            for channel in self.dct:
                for process in self.dct[channel]:
                    add = "ggH" if "ggH" in process else "xH"
                    f.write(
                        "shapes {} {} {} {} \n".format(
                            process,
                            channel,
                            self.dct[channel][process]["file"],
                            self.dct[channel][process]["ws"].replace(
                                "wsig_13TeV", "wsig_13TeV_{}".format(add)
                            )
                            if process != "OutsideAcceptance"
                            else self.dct[channel][process]["ws"],
                        )
                    )
            for channel in self.not_necessary:
                for process in self.not_necessary[channel]:
                    f.write(
                        "shapes {} {} {} {} \n".format(
                            process,
                            channel,
                            self.not_necessary[channel][process]["file"],
                            self.not_necessary[channel][process]["ws"].replace(
                                "wsig_13TeV", "wsig_13TeV_{}".format(add)
                            )
                            if process != "OutsideAcceptance"
                            else self.not_necessary[channel][process]["ws"],
                        )
                    )

            # Third part (bin, observation)
            f.write("-" * 100 + "\n")
            # print(self.processes)
            bin_str = "bin " + " ".join([channel for channel in self.dct]) + "\n"
            obs_string = "observation " + " ".join(["-1"] * len(self.dct)) + "\n"
            f.write(bin_str)
            f.write(obs_string)

            # Fourth part (bin, processes, processes, rate)
            f.write("-" * 100 + "\n")
            bin_str = (
                "bin "
                + " ".join(
                    [
                        "{} ".format(channel) * (len(self.dct[channel]) - 1)
                        for channel in self.dct
                    ]
                )
                + "\n"
            )
            process_str = (
                "process "
                + " ".join(
                    [
                        process
                        for channel in self.dct
                        for process in self.dct[channel]
                        if process != "data_obs"
                    ]
                )
                + "\n"
            )
            process_number_str = (
                "process "
                + " ".join(
                    [
                        str(self.dct[channel][process]["process_number"])
                        for channel in self.dct
                        for process in self.dct[channel]
                        if process != "data_obs"
                    ]
                )
                + "\n"
            )
            rate_str = (
                "rate "
                + " ".join(
                    [
                        str(self.dct[channel][process]["rate"])
                        for channel in self.dct
                        for process in self.dct[channel]
                        if process != "data_obs"
                    ]
                )
                + "\n"
            )
            for line in [
                bin_str,
                process_str,
                process_number_str,
                rate_str,
            ]:
                f.write(line)

            # Fifth part (systematics)
            f.write("-" * 100 + "\n")
            all_systematics = []
            for channel in self.dct:
                for process in self.dct[channel]:
                    if process != "data_obs":
                        for systematic in self.dct[channel][process]["systematics"]:
                            if systematic not in all_systematics:
                                all_systematics.append(systematic)
            for systematic in all_systematics:
                line = "{} lnN ".format(systematic) + " ".join(
                    [
                        self.dct[channel][process]["systematics"][systematic]
                        if systematic in self.dct[channel][process]["systematics"]
                        else "-"
                        for channel in self.dct
                        for process in self.dct[channel]
                        if process != "data_obs"
                    ]
                )
                f.write(line + "\n")

            # all the rest
            for line in self.other_lines:
                f.write(line)

            # """
            to_freeze = []
            for line in self.rate_params:
                rate_name, rate_param, channel, process, rate = line.split()
                rng = "_".join(process.split("_")[2:])
                rng = rng.replace("p0", "")
                factor = self.split["ggH_{}".format(rng)]
                # new_rate = float(rate) * factor
                new_rate = float(rate)
                rate_name = rate_name.replace("smH", "ggH")
                to_freeze.append(rate_name)
                new_line = "{} {} {} {} {}\n".format(
                    rate_name, rate_param, channel, process, new_rate
                )
                f.write(new_line)
                # xh_rate_name = rate_name.replace("ggH", "xH")
                # to_freeze.append(xh_rate_name)
                # xh_process = process.replace("ggH", "xH")
                # xh_factor = self.split["xH_{}".format(rng)]
                # xh_new_rate = float(rate) * xh_factor
                # xh_new_line = "{} {} {} {} {}\n".format(
                #    xh_rate_name, rate_param, channel, xh_process, xh_new_rate
                # )
                # f.write(xh_new_line)
            for param in to_freeze:
                f.write("nuisance edit freeze {} \n".format(param))
            # """


def main():
    # card = "DifferentialCombinationRun2/Analyses/hig-19-016/outdir_differential_Pt/test.txt"
    # card_out = "DifferentialCombinationRun2/Analyses/hig-19-016/outdir_differential_Pt/test_out.txt"
    cards = [
        "DifferentialCombinationRun2/Analyses/hig-19-016/outdir_differential_Pt/TK_Yukawa.txt",
        "DifferentialCombinationRun2/Analyses/hig-19-016/outdir_differential_Pt/TK_Top.txt",
    ]
    cards_out = [
        "DifferentialCombinationRun2/Analyses/hig-19-016/outdir_differential_Pt/TK_Yukawa_out.txt",
        "DifferentialCombinationRun2/Analyses/hig-19-016/outdir_differential_Pt/TK_Top_out.txt",
    ]

    for card, card_out in zip(cards, cards_out):
        dp = DatacardParser(card)
        dct_copy = deepcopy(dp.dct)
        for channel, channel_dct in dct_copy.items():
            for process, process_dct in channel_dct.items():
                if process.startswith("ggH") and process != "OutsideAcceptance":
                    # new_ws_name = process_dct["ws"].replace("smH", "xH")
                    new_ws_name = process_dct["ws"]
                    new_process_name = process.replace("ggH", "xH")
                    new_dct = deepcopy(process_dct["systematics"])
                    new_dct["CMS_xH_incxs"] = "1.02071"
                    dp.add_process(
                        process=new_process_name,
                        channel=channel,
                        file_path=process_dct["file"],
                        ws_name=new_ws_name,
                        is_signal=True,
                        rate=process_dct["rate"],
                        systematics=new_dct,
                    )
        print(json.dumps(dp.dct, indent=4))
        dp.write_card(card_out)


if __name__ == "__main__":
    main()

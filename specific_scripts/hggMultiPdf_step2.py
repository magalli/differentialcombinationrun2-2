"""
Run with 

python ../../../specific_scripts/hggMultiPdf_step2.py

from inside 

/work/gallim/DifferentialCombination_home/DifferentialCombinationRun2/Analyses/hig-19-016/outdir_differential_Pt

Note that this has to be run "all at once". If you re-run the same command on a file from which the indices have already been removed,
you will get a segmentation fault and a null pointer will probably be saved, inducing the following fits to fail miserably.
"""
import os

input_file = "multipdfs_used.txt"

info = []

with open(input_file, "r") as f:
    lines = f.readlines()
    for line in lines:
        if line.startswith("IDX") and "FAILED" not in line:
            words = line.split()
            sub_info = {}
            sub_info["index"] = words[1]
            sub_info["pdf"] = words[3]
            sub_info["workspace"] = words[5]
            sub_info["root_file"] = words[7]
            sub_info["keep"] = words[9].split(",")
            info.append(sub_info)
executable = "python /work/gallim/DifferentialCombination_home/rfwsutils/wsKeepDiscretePdfIndexes.py"

for dct in info:
    command_line = (
        " ".join([executable, dct["root_file"], dct["root_file"], dct["pdf"]])
        + " "
        + " ".join(dct["keep"])
    )
    os.system(command_line)


# Add xH nuisance uncertainty to Htt and HttBoost cards
def add_xh_line(input_card, output_card):
    with open(input_card, "r") as f:
        lines = f.readlines()
    new_lines = []
    for line in lines:
        line = line.replace(
            "kmax 125 number of nuisance parameters",
            "kmax 126 number of nuisance parameters",
        )
        line = line.replace(" XH", " xH")
        new_lines.append(line)
    for line in new_lines:
        if line.startswith("process"):
            processes = line.split()[1:]
            new_parts = []
            for p in processes:
                if p.startswith("xH"):
                    new_parts.append("1.02071")
                else:
                    new_parts.append("-")
            new_line = "CMS_xH_incxs lnN " + " ".join(new_parts)
            break
    with open(output_card, "w") as f:
        for line in new_lines:
            f.write(line)
        f.write(new_line)


input_cards = [
    "DifferentialCombinationRun2/Analyses/hig-20-015/HiggsPt/HTT_Run2FinalCard_HiggsPt_NoReg.txt",
    "DifferentialCombinationRun2/Analyses/hig-21-017/BoostedHTT_DiffXS_HiggsPt_NoOverLap/V2_Diff_dr0p5_hpt_2bin/hig-21-017_hpt.txt",
]

output_cards = [
    "DifferentialCombinationRun2/Analyses/hig-20-015/HiggsPt/HTT_Run2FinalCard_HiggsPt_NoReg_xHNuisPar.txt",
    "DifferentialCombinationRun2/Analyses/hig-21-017/BoostedHTT_DiffXS_HiggsPt_NoOverLap/V2_Diff_dr0p5_hpt_2bin/hig-21-017_hpt_xHNuisPar.txt",
]

for input_card, output_card in zip(input_cards, output_cards):
    add_xh_line(input_card, output_card)

"""
HWW inserted 'hww' in the process names, which lead to breaking the convention.
We thus write this script instead of wasting weeks asking the authors to change it.
Here we take all the ROOT files associated to the analysis and add to their TDirectoryFiles
new histogram with the correct name.

Run from the root level with 

python specific_scripts/rename_HWW.py

in an environtment which has Python3 and modern PyROOT
"""
import ROOT


base_dirs = ["Analyses/hig-19-002/ptH", "Analyses/hig-19-002/njet"]
root_files = ["templates_2016.root", "templates_2017.root", "templates_2018.root"]


def main():
    for base_dir in base_dirs:
        for root_file in root_files:
            fname = "{}/{}".format(base_dir, root_file)

            print("Working with file {}".format(fname))
            input_file = ROOT.TFile(fname, "UPDATE")

            lev1_keys = [key.GetName() for key in input_file.GetListOfKeys()]

            for lev1_key in lev1_keys:
                print("Working with TDirectory {}".format(lev1_key))
                tdirectory = input_file.Get(lev1_key)
                lev2_keys = [key.GetName() for key in tdirectory.GetListOfKeys()]
            
                for lev2_key in lev2_keys:
                    histo = tdirectory.Get(lev2_key)
                    old_name = histo.GetName()
                    old_title = histo.GetTitle()
                    new_name = old_name.replace("smH_hww", "smH")
                    new_title = old_title.replace("smH_hww", "smH")
                    
                    if new_name != old_title:
                        print("Changed: {} -> {}".format(old_name, new_name))
                        histo.SetName(new_name)
                        histo.SetTitle(new_title)
                        tdirectory.WriteObject(histo, histo.GetName())



if __name__ == "__main__":
    main()
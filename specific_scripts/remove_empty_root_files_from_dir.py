import ROOT
import argparse
import os

def parse_arguments():
    parser = argparse.ArgumentParser(description='Remove empty root files from a directory')
    parser.add_argument('-d', '--directory', help='Path to the directory', required=True)
    return parser.parse_args()

def main():
    args = parse_arguments()
    path = args.directory
    print('Removing empty root files from directory: {}'.format(path))
    for root, dirs, files in os.walk(path):
        for file in files:
            if file.endswith('.root'):
                file_path = os.path.join(root, file)
                f = ROOT.TFile(file_path)
                try:
                    t = f.limit
                    f.Close()
                except AttributeError:
                    os.remove(file_path)
                    print('Removed file: {}'.format(file_path))

if __name__ == '__main__':
    main()
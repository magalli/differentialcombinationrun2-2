"""
Run with pyhon 3 from /work/gallim/DifferentialCombination_home/DifferentialCombinationRun2
Remember that tophighpt_ct_0p1_cg_0p075_cb_1_muR_1_muF_1_Q_1.txt and tophighpt_ct_0p5_cg_0p042_cb_1_muR_1_muF_1_Q_1.txt are manually updated
"""
import os

input_dir = "TKPredictions/theories_tophighpt"
sm_file = "tophighpt_ct_1_cg_0_cb_1_muR_1_muF_1_Q_1.txt"


def linear_interpolation(x1, y1, x2, y2, x):
    y = y1 + (x - x1) * (y2 - y1) / (x2 - x1)
    return y


def readline(line):
    feature, lst_with_commas = line.split("=")
    # drop new line
    lst_with_commas = lst_with_commas.replace("\n", "")
    lst = lst_with_commas.split(",")
    try:
        lst = [float(x) for x in lst]
    except ValueError:
        pass
    return feature, lst


def centers_from_edges(edges):
    centers = []
    for i in range(len(edges) - 1):
        centers.append((edges[i] + edges[i + 1]) / 2)
    return centers


def txt_file_from_features(features):
    new_lines = []
    for feature in features:
        lst_string = ",".join([str(x) for x in features[feature]])
        new_lines.append(f"{feature}={lst_string}\n")
    return new_lines


def do_operations_no_ratio(path_to_file):
    features = {}
    old_lines = []
    with open(path_to_file) as fl:
        for line in fl.readlines():
            old_lines.append(line)
            feature, lst = readline(line)
            features[feature] = lst
    # print(features)
    # print(len(features["binBoundaries"]))
    # print(len(features["binCenters"]))
    # get index of 675.0 in bincenters
    old_left_index = features["binBoundaries"].index(670.0)
    old_right_index = features["binBoundaries"].index(680.0)
    index_center = features["binCenters"].index(675.0)
    new_xs_672_5 = linear_interpolation(
        features["binCenters"][old_left_index],
        features["crosssection"][old_left_index],
        features["binCenters"][old_right_index],
        features["crosssection"][old_right_index],
        672.5,
    )
    new_xs_677_5 = linear_interpolation(
        features["binCenters"][old_left_index],
        features["crosssection"][old_left_index],
        features["binCenters"][old_right_index],
        features["crosssection"][old_right_index],
        677.5,
    )
    new_xs_int_672_5 = linear_interpolation(
        features["binCenters"][old_left_index],
        features["crosssection_integrated"][old_left_index],
        features["binCenters"][old_right_index],
        features["crosssection_integrated"][old_right_index],
        672.5,
    )
    new_xs_int_677_5 = linear_interpolation(
        features["binCenters"][old_left_index],
        features["crosssection_integrated"][old_left_index],
        features["binCenters"][old_right_index],
        features["crosssection_integrated"][old_right_index],
        677.5,
    )
    features["binBoundaries"].insert(index_center + 1, 675.0)
    features["binCenters"] = centers_from_edges(features["binBoundaries"])

    # now remove old value and insert new ones
    features["crosssection"].pop(index_center)
    features["crosssection"].insert(index_center, new_xs_672_5)
    features["crosssection"].insert(index_center + 1, new_xs_677_5)
    features["crosssection_integrated"].pop(index_center)
    features["crosssection_integrated"].insert(index_center, new_xs_int_672_5)
    features["crosssection_integrated"].insert(index_center + 1, new_xs_int_677_5)

    features["ratios"] += [1.0]

    return features


output_dir = "TKPredictions/theories_tophighpt_mod"

# sm
sm_features = do_operations_no_ratio(os.path.join(input_dir, sm_file))
# print(sm_features)
sm_new_lines = txt_file_from_features(sm_features)
with open(os.path.join(output_dir, sm_file), "w") as fl:
    fl.writelines(sm_new_lines)

for file in os.listdir(input_dir):
    try:
        if file == sm_file:
            continue
        features = do_operations_no_ratio(os.path.join(input_dir, file))
        features["ratios"] = [
            x / y for x, y in zip(features["crosssection"], sm_features["crosssection"])
        ]
        # print(features)
        new_lines = txt_file_from_features(features)
        with open(os.path.join(output_dir, file), "w") as fl:
            fl.writelines(new_lines)
    except ValueError:  # 675 not in binCenters
        print(f"Skipping {file}")
        continue

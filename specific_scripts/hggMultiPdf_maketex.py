"""
python3
Use a multipdfs_used.txt as input file to print a nice tex table with the multipdfs used in the analysis
It has to be run from the directory where the multipdfs_used.txt is located.
"""
from pylatex import Table, Tabular, Document, LongTable


def main():
    input_file = "multipdfs_used.txt"
    output_file = "multipdfs_used"

    table = LongTable("|l|c|")
    table.add_hline()
    table.add_row(("Category", "Ratio of used to total (%)"))
    table.add_hline()

    with open(input_file, "r") as f:
        for line in f:
            if not line.startswith("IDX"):
                continue
            if "FAILED" in line:
                continue
            words = line.split()
            idx = words[1]
            parts = idx.split("_")
            category = "_".join(parts[3:])
            table.add_row((category, words[-2]))
            table.add_hline()

    table.generate_tex(output_file)


if __name__ == "__main__":
    main()

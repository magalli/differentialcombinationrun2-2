"""
# Run with Python3

Fits can fail for multiple reasons. Usually when it happens we see it from one (or more) point in the scan 
parabola that is completely out. When running 'plot_xs_scans.py' in debug mode, the origin al points are printed
and it is easy to see which points are problematic.
"""
import argparse
import glob
import os
import uproot
import awkward as ak


def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Remove failed fits ROOT files from a scan directory"
    )
    parser.add_argument(
        "--input-dir",
        type=str,
        required=True,
        help="The input directory from which to remove failed fits",
    )
    parser.add_argument(
        "--poi", type=str, required=True, help="The POI for which to remove failed fits"
    )
    parser.add_argument(
        "--remove", action="store_true", default=False, help="Remove the failed fits"
    )
    parser.add_argument(
        "--file-name-tmpl",
        type=str,
        required=False,
        default=None,
        help="File name template in case of 2D scans to plot the 1D ones, since the convention is different from the combination ones",
    )
    return parser.parse_args()


def main(args):
    input_dir = args.input_dir
    poi = args.poi

    if args.file_name_tmpl:
        file_name_tmpl = args.file_name_tmpl
    else:
        file_name_tmpl = "higgsCombine_SCAN_{}*.root".format(poi)
    tree_name = "limit"

    files = glob.glob(f"{input_dir}/{file_name_tmpl}")
    files = sorted(files, key=str.lower)

    for file in files:
        f = uproot.open(file)
        tree = f[tree_name]
        values = tree.arrays(expressions=[poi, "deltaNLL"])
        pois = values[poi].to_list()
        deltaNLL = values["deltaNLL"].to_list()
        print(file)
        print(f"{poi}: {pois}")
        print(f"deltaNLL: {deltaNLL}")
        if args.remove:
            if len(pois) == 0 and len(deltaNLL) == 0:
                print(f"Removing {file}")
                os.remove(file)


if __name__ == "__main__":
    args = parse_arguments()
    main(args)

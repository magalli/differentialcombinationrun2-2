import os
import ROOT

old_name = "OutsideAcceptance"
new_name = "OutsideAcceptanceHZZ"
old_name_function = "OutsideAcceptance_norm"
new_name_function = "OutsideAcceptanceHZZ_norm"

# Renaming pdfs
print("Renaming processes")
input_dirs = [
    #"DifferentialCombinationRun2/Analyses/hig-21-009/pT4l",
    #"DifferentialCombinationRun2/Analyses/hig-21-009/njets_pt30_eta4p7",
    #"DifferentialCombinationRun2/Analyses/hig-21-009/rapidity4l",
    #"DifferentialCombinationRun2/Analyses/hig-21-009/pTj1",
    #"DifferentialCombinationRun2/Analyses/hig-21-009/mjj",
    ## "DifferentialCombinationRun2/Analyses/hig-21-009/costhetastar",
    #"DifferentialCombinationRun2/Analyses/hig-21-009/TCjmax",
    #"DifferentialCombinationRun2/Analyses/hig-21-009/pTj2",
    #"DifferentialCombinationRun2/Analyses/hig-21-009/absdetajj",
    "DifferentialCombinationRun2/Analyses/hig-21-009/dphijj"
]
dc_dirs = ["datacard_2016", "datacard_2017", "datacard_2018"]

for input_dir in input_dirs:
    for dc_dir in dc_dirs:
        root_files = [
            "{}/{}/{}".format(input_dir, dc_dir, f)
            for f in os.listdir("{}/{}".format(input_dir, dc_dir))
            if f.endswith(".root") and f.startswith("hzz4l")
        ]
        for fname in root_files:
            print("Analyzing {}".format(fname))
            f = ROOT.TFile(fname)
            w = f.w
            pdf = w.pdf(old_name)
            if pdf != None:
                print("Renaming {}".format(old_name))
                pdf.SetNameTitle(new_name, new_name)
                # clone = pdf.Clone(old_name)
                # clone.SetNameTitle(new_name, new_name)
                # getattr(w, "import")(clone)
            function = w.function(old_name_function)
            if function != None:
                print("Renaming {}".format(old_name_function))
                function.SetNameTitle(new_name_function, new_name_function)
            w.writeToFile(fname)

# Renaming cards
print("Renaming cards")

cards = [
    #"DifferentialCombinationRun2/Analyses/hig-21-009/pT4l/hzz4l_all_13TeV_xs_pT4l_bin_v3.txt",
    #"DifferentialCombinationRun2/Analyses/hig-21-009/njets_pt30_eta4p7/hzz4l_all_13TeV_xs_njets_pt30_eta4p7_bin_v3.txt",
    #"DifferentialCombinationRun2/Analyses/hig-21-009/rapidity4l/hzz4l_all_13TeV_xs_rapidity4l_bin_v3.txt",
    #"DifferentialCombinationRun2/Analyses/hig-21-009/pTj1/hzz4l_all_13TeV_xs_pTj1_bin_v3.txt",
    #"DifferentialCombinationRun2/Analyses/hig-21-009/mjj/hzz4l_all_13TeV_xs_mjj_bin_v3.txt",
    ## "DifferentialCombinationRun2/Analyses/hig-21-009/costhetastar/hzz4l_all_13TeV_xs_costhetastar_bin_v4.txt",
    #"DifferentialCombinationRun2/Analyses/hig-21-009/TCjmax/hzz4l_all_13TeV_xs_TCjmax_bin_v3.txt",
    #"DifferentialCombinationRun2/Analyses/hig-21-009/pTj2/hzz4l_all_13TeV_xs_pTj2_bin_v3.txt",
    #"DifferentialCombinationRun2/Analyses/hig-21-009/absdetajj/hzz4l_all_13TeV_xs_absdetajj_bin_v3.txt",
    "DifferentialCombinationRun2/Analyses/hig-21-009/dphijj/hzz4l_all_13TeV_xs_dphijj_bin_v3.txt"
]

for card in cards:
    with open(card, "r") as f:
        lines = f.readlines()

    with open(card, "w") as f:
        for line in lines:
            if "OutsideAcceptance" in line:
                line = line.replace("OutsideAcceptance ", "OutsideAcceptanceHZZ ")
            f.write(line)

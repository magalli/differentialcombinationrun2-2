"""
To be run with python2
"""
import ROOT

to_replace = [
    "testModel_2016.root",
    "testModel_2016APV.root",
    "testModel_2017.root",
    "testModel_2018.root",
]
prefix = "/work/gallim/DifferentialCombination_home/DifferentialCombinationRun2/Analyses/hig-21-020/testModel"
card_path = (
    "DifferentialCombinationRun2/Analyses/hig-21-020/testModel/model_combined.txt"
)
output_path = "DifferentialCombinationRun2/Analyses/hig-21-020/testModel/model_combined_withpaths.txt"

maps = {
    "ptbin0mjjbin0ggf": "ggH_PTH_450_500",
    "ptbin1mjjbin0ggf": "ggH_PTH_500_550",
    "ptbin2mjjbin0ggf": "ggH_PTH_550_600",
    "ptbin3mjjbin0ggf": "ggH_PTH_600_675",
    "ptbin4mjjbin0ggf": "ggH_PTH_675_800",
    "ptbin5mjjbin0ggf": "ggH_PTH_800_1200",
}

# Now rename in the root files
for rp in to_replace:
    ws_name = rp.replace(".root", "")
    root_file_name = "{}/{}".format(prefix, rp)
    f = ROOT.TFile(root_file_name)
    w = f.Get(ws_name)
    for bin_name, new_process_name in maps.items():
        old_name = "{}_ggF".format(bin_name)
        new_name = "{}_{}".format(bin_name, new_process_name)
        pdf = w.pdf(old_name)
        if pdf != None:
            print(
                "Found {} in {}:{}, renaming".format(old_name, root_file_name, ws_name)
            )
            pdf.SetNameTitle(new_name, new_name)
        w.writeToFile(root_file_name)

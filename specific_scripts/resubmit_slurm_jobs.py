"""
To be run from inside the directory where jobs and ROOT files are stored, it checks for jobs failed due to time limit and resubmit 
in the long partition.
"""
import os


def main():
    logs = [f for f in os.listdir(".") if f.endswith(".log")]

    failed_jobs = []
    for log in logs:
        with open(log) as fl:
            text = fl.read()
        if "slurmstepd: error" in text:
            failed_jobs.append(log.split(".log")[0])
    print("{}/{} jobs failed".format(len(failed_jobs), len(logs)))
    print("\n".join(failed_jobs))

    for job in failed_jobs:
        name = "_".join(job.split("_")[:-1]) + ".sh"
        print("Resubmitting {}".format(name))
        os.system("sbatch --mem=20G --partition=long --time=2-00:00:00 {}".format(name))


if __name__ == "__main__":
    main()

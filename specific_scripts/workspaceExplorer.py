"""
Some crap taken from https://gitlab.cern.ch/cms-hcg/comb/summer21/-/blob/master/workspaceExplorer.py
"""

import ROOT
import argparse
import re
from collections import defaultdict

ROOT.gSystem.Load("libHiggsAnalysisCombinedLimit.so")
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(ROOT.kTRUE)
ROOT.TH1.AddDirectory(False)

parser = argparse.ArgumentParser()
parser.add_argument('-w', '--workspace', help='Input workspace in the format FILE:WORKSPACE')
parser.add_argument('-p', '--pdf', default=None, help='PDF to check')
parser.add_argument('--optimize', default=0, type=int, help='optimisations to apply')
parser.add_argument('-o', '--output', help='Output workspace', default='out.root')
args = parser.parse_args()

filename, wspname = args.workspace.split(':')
f = ROOT.TFile(filename)
w = f.Get(wspname)

# Optimisation strategies?
# 1) Replace RooFormulaVar with compiled equivalent
# 2) Eliminate RooConstVar's where possible
# 3) Look for common sub-expression elimination in RooFormulaVars (identical formulae and identical args)
# 4) Can replace a Formula that depends only on: other formulae, constvars and realvars with a single formula

cpp_FormulaStr="""
std::string GetFormulaCPP(RooFormulaVar *var) {
    std::stringstream ss;
    var->printMetaArgs(ss);
    return ss.str();
}
"""
ROOT.gInterpreter.ProcessLine(cpp_FormulaStr)

cpp_FormulaStrGenPdf="""
std::string GetFormulaGenCPP(RooGenericPdf *form) {
    std::stringstream ss;
    form->printMetaArgs(ss);
    return ss.str();
}
"""
ROOT.gInterpreter.ProcessLine(cpp_FormulaStrGenPdf)


def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)


def GetTypesDict(inputs):
    res = {}
    for X in inputs.split(','):
        if X == '':
            continue
        res[int(X[1:-3])] = X[-3:]
    return res


def FormulaIsSimpleProduct(formula):
    split = formula.split('*')
    ok = True
    for item in split:
        x = item.strip()
        res = re.match('\\@\\d+', x)
        if not res:
            ok = False
            break
    return ok

def GetFormulaType(formula, inputs):
    indict = GetTypesDict(inputs)
    # Simple product
    if FormulaIsSimpleProduct(formula):
        return 'SimpleProduct'
    # Type A
    prepost = ('(@0+@1)*(1.+@2+@3+', ')')
    if formula.startswith(prepost[0]) and formula.endswith(prepost[1]) and (indict[0] == indict[2] == indict[3] == '[V]'):
        stripped = formula[len(prepost[0]):-1*len(prepost[1])].split('+')
        ok = True
        ibegin = 4
        for s in stripped:
            res = re.match('\\@(\\d+)\\*\\@(\\d+)', s)
            if res:
                vals = [int(X) for X in res.group(1, 2)]
                if not (vals[0] == ibegin and vals[1] == ibegin + 1 and indict[vals[0]] == '[C]' and indict[vals[1]] == '[V]'):
                    ok = False
                    break
                else:
                    ibegin += 2
            else:
                ok = False
                break
        if ok:
            if indict[1] == '[V]':
                return 'TypeA1'
            elif indict[1] == '[C]':
                return 'TypeA2'
    # Type B
    prepost = ('@0*TMath::Max(1.e-2,(1.+', '))')
    if formula.startswith(prepost[0]) and formula.endswith(prepost[1]):
        stripped = formula[len(prepost[0]):-1*len(prepost[1])].split('+')
        ok = True
        ibegin = 1
        for s in stripped:
            res = re.match('\\@(\\d+)\\*\\@(\\d+)', s)
            if res:
                vals = [int(X) for X in res.group(1, 2)]
                if not (vals[0] == ibegin and vals[1] == ibegin + 1 and indict[vals[0]] == '[V]' and indict[vals[1]] == '[C]'):
                    ok = False
                    break
                else:
                    ibegin += 2
            else:
                ok = False
                break
        if ok:
            if indict[0] == '[V]':
                return 'TypeB1'
            elif indict[0] == '[C]':
                return 'TypeB2'
    # Type C
    prepost = ('(1.+', ')')
    if formula.startswith(prepost[0]) and formula.endswith(prepost[1]):
        stripped = formula[len(prepost[0]):-1*len(prepost[1])].split('+')
        ok = True
        ibegin = 0
        for s in stripped:
            res = re.match('\\@(\\d+)\\*\\@(\\d+)', s)
            if res:
                vals = [int(X) for X in res.group(1, 2)]
                if not (vals[0] == ibegin and vals[1] == ibegin + 1 and indict[vals[0]] == '[C]' and indict[vals[1]] == '[V]'):
                    ok = False
                    break
                else:
                    ibegin += 2
            else:
                ok = False
                break
        if ok:
            return 'TypeC1'
    # Type D
    if formula == 'TMath::Min(@0+@1,1.0)':
        if indict[0] == '[V]' and indict[1] == '[V]':
            return 'TypeD1'
        if indict[0] == '[V]' and indict[1] == '[C]':
            return 'TypeD2'
    return None


def BuildSimpleProduct(obj, typename):
    print 'Building %s: %s' % (typename, obj.GetName())
    v = ROOT.RooArgList(obj.formula().actualDependents())
    newarg = ROOT.RooCheapProduct(obj.GetName(), '', v, True)
    print obj.getVal(), newarg.getVal()
    res = {
        'newarg': newarg,
        'v': v,
    }
    return res


def BuildTypeA(obj, typename):
    print 'Building %s: %s' % (typename, obj.GetName())
    v = ROOT.RooArgList(obj.formula().actualDependents())
    var0 = v.at(0)
    var1 = v.at(1)
    if typename == 'TypeA2':
        var1 = v.at(1).getVal()
    var2 = v.at(2)
    var3 = v.at(3)
    n_prod_terms = (int(v.getSize()) - 4) / 2
    coeffs = ROOT.std.vector(ROOT.double)(n_prod_terms)
    terms = ROOT.RooArgList()
    for i in range(n_prod_terms):
        v.at(4 + 2 * i).Print()
        v.at(5 + 2 * i).Print()
        coeffs[i] = v.at(4 + 2 * i).getVal()
        terms.add(v.at(5 + 2 * i))
    if typename == 'TypeA1':
        newarg = ROOT.CMSHggFormulaA1(obj.GetName(), '', var0, var1, var2, var3, terms, coeffs)
    elif typename == 'TypeA2':
        newarg = ROOT.CMSHggFormulaA2(obj.GetName(), '', var0, var1, var2, var3, terms, coeffs)

    print obj.getVal(), newarg.getVal()
    res = {
        'newarg': newarg,
        'var0': var0,
        'var1': var1,
        'var2': var2,
        'var3': var3,
        'coeffs': coeffs,
        'terms': terms
    }
    return res


def BuildTypeB(obj, typename):
    print 'Building %s: %s' % (typename, obj.GetName())
    v = ROOT.RooArgList(obj.formula().actualDependents())
    var0 = v.at(0)
    if typename == 'TypeB2':
        var0 = v.at(0).getVal()
    n_prod_terms = (int(v.getSize()) - 1) / 2
    coeffs = ROOT.std.vector(ROOT.double)(n_prod_terms)
    terms = ROOT.RooArgList()
    for i in range(n_prod_terms):
        coeffs[i] = v.at(2 + 2 * i).getVal()
        terms.add(v.at(1 + 2 * i))
    if typename == 'TypeB1':
        newarg = ROOT.CMSHggFormulaB1(obj.GetName(), '', var0, terms, coeffs)
    elif typename == 'TypeB2':
        newarg = ROOT.CMSHggFormulaB2(obj.GetName(), '', var0, terms, coeffs)
    print obj.getVal(), newarg.getVal()
    res = {
        'newarg': newarg,
        'var0': var0,
        'coeffs': coeffs,
        'terms': terms
    }
    return res


def BuildTypeC(obj, typename):
    print 'Building %s: %s' % (typename, obj.GetName())
    v = ROOT.RooArgList(obj.formula().actualDependents())
    n_prod_terms = (int(v.getSize())) / 2
    coeffs = ROOT.std.vector(ROOT.double)(n_prod_terms)
    terms = ROOT.RooArgList()
    for i in range(n_prod_terms):
        v.at(0 + 2 * i).Print()
        v.at(1 + 2 * i).Print()
        coeffs[i] = v.at(0 + 2 * i).getVal()
        terms.add(v.at(1 + 2 * i))
    newarg = ROOT.CMSHggFormulaC1(obj.GetName(), '', terms, coeffs)
    print obj.getVal(), newarg.getVal()
    res = {
        'newarg': newarg,
        'coeffs': coeffs,
        'terms': terms
    }
    return res


def BuildTypeD(obj, typename):
    print 'Building %s: %s' % (typename, obj.GetName())
    v = ROOT.RooArgList(obj.formula().actualDependents())
    v0 = v.at(0)
    if typename == 'TypeD1':
        v1 = v.at(1)
        newarg = ROOT.CMSHggFormulaD1(obj.GetName(), '', v0, v1)
    if typename == 'TypeD2':
        v1 = v.at(1).getVal()
        newarg = ROOT.CMSHggFormulaD2(obj.GetName(), '', v0, v1)
    print obj.getVal(), newarg.getVal()
    res = {
        'newarg': newarg,
    }
    return res


def BuildSimpleProdPdf(obj):
    newarg = ROOT.SimpleProdPdf(obj.GetName(), '', obj)
    res = {
        'newarg': newarg
    }
    return res


def GetFormula(formula):
    return ROOT.GetFormulaCPP(formula)[9:-2]

def GetFormulaGen(formula):
    return ROOT.GetFormulaGenCPP(formula)[9:-2]



def CollectTypes(arg, typecounts, commonformulae, commongenpdfs):
    alldeps = ROOT.RooArgList()
    arg.treeNodeServerList(alldeps, 0, True, True, False, True)
    dep_it = alldeps.createIterator()
    dep = dep_it.Next()
    while dep != None:
        typecounts[dep.ClassName()].add(dep)
        if dep.ClassName() == 'RooFormulaVar':
            formula = GetFormula(dep)
            form_alldeps = ROOT.RooArgList(dep.formula().actualDependents())
            names = []
            for ir in range(form_alldeps.getSize()):
                if form_alldeps.at(ir).ClassName() == 'RooConstVar':
                    names.append('@%i[C]' % ir)
                else:
                    names.append('@%i[V]' % ir)
            fullform = str(formula) + ';;' + ','.join(names)
            commonformulae[fullform].add(dep)
        if dep.ClassName() == 'RooGenericPdf':
            formula = GetFormulaGen(dep)
            form_alldeps = ROOT.RooArgSet()
            dep.getDependents(form_alldeps)
            names = []
            for ir in range(form_alldeps.getSize()):
                if form_alldeps.at(ir).ClassName() == 'RooConstVar':
                    names.append('@%i[C]' % ir)
                else:
                    names.append('@%i[V]' % ir)
            fullform = str(formula) + ';;' + ','.join(names)
            commongenpdfs[fullform].add(dep)
        dep = dep_it.Next()

analysedpdfs = list()
typecounts = defaultdict(set)
commonformulae = defaultdict(set)
commongenpdfs = defaultdict(set)


if args.pdf is not None:
    analysedpdfs.append(args.pdf)
    CollectTypes(w.pdf(args.pdf), typecounts, commonformulae, commongenpdfs)
else:
    allPdfs = w.allPdfs()
    pdf_it = allPdfs.createIterator()
    pdf = pdf_it.Next()
    while pdf != None:
        # Only take top-level pdfs
        if not pdf.hasClients():
            analysedpdfs.append(pdf.GetName())
            CollectTypes(pdf, typecounts, commonformulae, commongenpdfs)
            norm = w.function(pdf.GetName() + '_norm')
            if norm != None:
                analysedpdfs.append(norm.GetName())
                CollectTypes(norm, typecounts, commonformulae, commongenpdfs)
        pdf = pdf_it.Next()

out = ROOT.RooWorkspace(wspname, wspname)

to_add = []

print '%-30s : %s' % ('Input', args.workspace)
print '%-30s :' % 'PDFs'
for p in analysedpdfs:
    print ' - %s' % p
print '%-30s :' % 'Object type frequency'
for t in sorted(typecounts.iteritems(), key=lambda x: len(x[1])):
    print ' - %-4i %-30s' % (len(t[1]), t[0])
print '%-30s :' % 'Common RooFormulaVar frequency'
for f in sorted(commonformulae.iteritems(), key=lambda x: len(x[1])):
    form, inputs = f[0].split(';;')
    print ' - %-4i %s %s' % (len(f[1]), form, inputs)
    # for obj in f[1]:
    #     obj.Print('v')
    formtype = GetFormulaType(form, inputs)
    if args.optimize == 1:
        if formtype in ['TypeA1', 'TypeA2']:
            for obj in f[1]:
                to_add.append(BuildTypeA(obj, formtype))
                # getattr(out,'import')(BuildTypeA(obj, formtype)['newarg'], ROOT.RooFit.RecycleConflictNodes(), ROOT.RooFit.Silence())
        if formtype in ['TypeB1', 'TypeB2']:
            for obj in f[1]:
                to_add.append(BuildTypeB(obj, formtype))
                # getattr(out,'import')(BuildTypeB(obj, formtype)['newarg'], ROOT.RooFit.RecycleConflictNodes(), ROOT.RooFit.Silence())
        if formtype in ['TypeC1']:
            for obj in f[1]:
                # to_add.append(BuildTypeC(obj, formtype))
                to_add.insert(0, BuildTypeC(obj, formtype))
                # getattr(out,'import')(BuildTypeC(obj, formtype)['newarg'], ROOT.RooFit.RecycleConflictNodes(), ROOT.RooFit.Silence())
        if formtype in ['TypeD1', 'TypeD2']:
            for obj in f[1]:
                to_add.insert(0, BuildTypeD(obj, formtype))
                # getattr(out,'import')(BuildTypeD(obj, formtype)['newarg'], ROOT.RooFit.RecycleConflictNodes(), ROOT.RooFit.Silence())
        # This ordering is important...
        if formtype in ['SimpleProduct']:
            for obj in f[1]:
                to_add.append(BuildSimpleProduct(obj, formtype))
                # getattr(out,'import')(BuildSimpleProduct(obj, formtype)['newarg'], ROOT.RooFit.RecycleConflictNodes(), ROOT.RooFit.Silence())
print '%-30s :' % 'Common RooGenericPdf frequency'
for f in sorted(commongenpdfs.iteritems(), key=lambda x: len(x[1])):
    form, inputs = f[0].split(';;')
    print ' - %-4i %s %s' % (len(f[1]), form, inputs)

# Optimize RooProdPdfs
if 'RooProdPdf' in typecounts and args.optimize == 2:
    obs = w.data('data_obs').get()
    for prodpdf in typecounts['RooProdPdf']:
        to_add.append(BuildSimpleProdPdf(prodpdf))
        origval = prodpdf.getVal(obs)
        newval = to_add[-1]['newarg'].getVal(obs)
        text = 'OK'
        if not isclose(origval, newval):
            text = 'NOT OK'
        print '%-70s %-15f %-15f %s' %(prodpdf.GetName(), origval, newval, text)

for obj in to_add:
    print 'Importing %s (%s)' % (obj['newarg'].GetName(), obj['newarg'].ClassName())
    getattr(out, 'import')(obj['newarg'], ROOT.RooFit.RecycleConflictNodes(), ROOT.RooFit.Silence())

if args.optimize == 2:
    # Need to copy data for HZZ
    allData = list(w.allData())
    for dat in allData:
        getattr(out, 'import')(dat, ROOT.RooCmdArg())

if args.optimize >= 1:
    for pdf in analysedpdfs:
        print 'Importing %s' % pdf
        getattr(out,'import')(w.function(pdf), ROOT.RooFit.RecycleConflictNodes(), ROOT.RooFit.Silence())
    out.writeToFile(args.output)

del out

import numpy as np
import pickle
import json

from print_mappings import get_prediction


def main():
    hgg_br = 0.0023
    mass = 125.38
    weights = [1.0, 2.3, 1.0]

    input_files = {
        "ggh": "../TheoreticalPredictions/production_modes/ggH/theoryPred_Pt_2018_ggHMoreGranular.pkl",
        "vbf": "../TheoreticalPredictions/production_modes/vbf/theoryPred_Pt_2018_ggHMoreGranular.pkl",
        "vh": "../TheoreticalPredictions/production_modes/vh/theoryPred_Pt_2018_ggHMoreGranular.pkl",
        "tth": "../TheoreticalPredictions/production_modes/ttH/theoryPred_Pt_2018_ttHJetToGG.pkl",
    }

    binning = [
        0.0,
        5.0,
        10.0,
        15.0,
        20.0,
        25.0,
        30.0,
        35.0,
        45.0,
        60.0,
        80.0,
        100.0,
        120.0,
        140.0,
        170.0,
        200.0,
        250.0,
        350.0,
        450.0,
        10000.0,
    ]

    predictions = {}
    for proc, input_file in input_files.items():
        with open(input_file, "rb") as f:
            arr = pickle.load(f)
        predictions[proc] = get_prediction(arr, mass, weights=weights)
    # tot
    predictions["tot"] = np.sum([v for v in predictions.values()], axis=0)
    new_predictions = {}
    new_predictions["ggH"] = predictions["ggh"] / predictions["tot"]
    new_predictions["xH"] = (
        predictions["vbf"] + predictions["vh"] + predictions["tth"]
    ) / predictions["tot"]

    output = {}
    for i, (left, right) in enumerate(zip(binning[:-1], binning[1:])):
        process = f"{left}_{right}".replace(".", "p")
        process = process.replace("p0", "")
        output["ggH_{}".format(process)] = new_predictions["ggH"][i]
        output["xH_{}".format(process)] = new_predictions["xH"][i]
        # output[process] = {}
        # output[process]["ggH"] = new_predictions["ggH"][i]
        # output[process]["xH"] = new_predictions["xH"][i]
    print(output)

    # dump to json
    with open("../TKPredictions/hgg.json", "w") as f:
        json.dump(output, f, indent=4)


if __name__ == "__main__":
    main()

import ROOT
import json
import os
import re

input_dir = "/work/gallim/DifferentialCombination_home/DifferentialCombinationRun2/Analyses/hig-19-016/outdir_differential_Pt"
# list all ROOT files
files = [
    f
    for f in os.listdir(input_dir)
    if f.endswith(".root") and "sigfit" in f and "OutsideAcceptance" not in f
]

# debug
# files = ["CMS-HGG_sigfit_smH_PTH_80p0_100p0_16.root"]

with open("../../DifferentialCombinationRun2/TKPredictions/hgg.json", "r") as f:
    split = json.load(f)
print(split.keys())

for f in files:
    print("\nProcessing file {}".format(f))
    full_path = os.path.join(input_dir, f)
    rf = ROOT.TFile(full_path, "UPDATE")
    w = rf.wsig_13TeV
    w_ggh = w.Clone()
    w_ggh.SetNameTitle("wsig_13TeV_ggH", "wsig_13TeV_ggH")
    w_xh = w.Clone()
    w_xh.SetNameTitle("wsig_13TeV_xH", "wsig_13TeV_xH")

    productions = ["ggH", "xH"]
    workspaces = [w_ggh, w_xh]

    for prod_name, ws in zip(productions, workspaces):
        print("\nProcessing production {}".format(prod_name))
        print(ws.GetName())
        function_names = []
        all_functions = ws.allFunctions()
        iterator = all_functions.createIterator()
        function = iterator.Next()
        while function:
            function_names.append(function.GetName())
            function = iterator.Next()

        xs_function_names = [f for f in function_names if f.startswith("fxs_smH_PTH")]
        print(xs_function_names)
        for xs_name in xs_function_names:
            xs_original = ws.function(xs_name)
            xs_original.SetNameTitle(xs_name + "_old", xs_name + "_old")
            xs_original = xs_original.Clone()
            new_name = xs_name + "_original"
            xs_original.SetNameTitle(new_name, new_name)
            getattr(ws, "import")(xs_original)
            pattern = "(\d+p0_\d+p0)"
            rng = re.findall(pattern, xs_name)[0]
            rng = rng.replace("p0", "")
            print("Range: {}".format(rng))
            string_in_dict = "{}_{}".format(prod_name, rng)
            value = split[string_in_dict]
            ws.factory('expr::{}("{}*@0", {})'.format(xs_name, value, new_name))
            new_xs = ws.function(xs_name)
            getattr(ws, "import")(new_xs)
        ws.Write()
    rf.Close()
